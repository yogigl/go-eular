package p001

import "testing"

func TestP001(t *testing.T) {
	result := p001()
	expected := 233168

	if result != expected {
		t.Errorf("expected '%d' but got '%d' ", expected, result)
	}
}
